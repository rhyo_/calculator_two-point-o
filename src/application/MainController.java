package application;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

public class MainController implements Initializable {

	static math.tree.MathTree calcTree = new math.tree.MathTree();
	boolean clearText = false;

	@FXML
	private Button one;
	@FXML
	private Button two;
	@FXML
	private Button three;
	@FXML
	private Button four;
	@FXML
	private Button five;
	@FXML
	private Button six;
	@FXML
	private Button seven;
	@FXML
	private Button eight;
	@FXML
	private Button nine;
	@FXML
	private Button zero;
	@FXML
	private Button point;
	@FXML
	private Button add;
	@FXML
	private Button sub;
	@FXML
	private Button mult;
	@FXML
	private Button div;
	@FXML
	private Button gleich;
	@FXML
	private Button mod;
	@FXML
	private Button parenthesis;
	@FXML
	private Button reset;
	@FXML
	private Button del;
	@FXML
	private TextField display;

	@FXML
	public void handleButtonAction(ActionEvent event) {
		Object ButtonValue = event.getSource();
		if (clearText == true) {
			display.clear();
			clearText = false;
		}

		if (ButtonValue == one) {
			display.setText(display.getText() + "1");
		} else if (ButtonValue == two) {
			display.setText(display.getText() + "2");
		} else if (ButtonValue == three) {
			display.setText(display.getText() + "3");
		} else if (ButtonValue == four) {
			display.setText(display.getText() + "4");
		} else if (ButtonValue == five) {
			display.setText(display.getText() + "5");
		} else if (ButtonValue == six) {
			display.setText(display.getText() + "6");
		} else if (ButtonValue == seven) {
			display.setText(display.getText() + "7");
		} else if (ButtonValue == eight) {
			display.setText(display.getText() + "8");
		} else if (ButtonValue == nine) {
			display.setText(display.getText() + "9");
		} else if (ButtonValue == zero) {
			display.setText(display.getText() + "0");
		} else if (ButtonValue == point) {
			if (display.getText().charAt(display.getText().length() - 1) != '.') {
				display.setText(display.getText() + ".");
			}
		} else if (ButtonValue == parenthesis) {
			if (display.getText().contains("(")) {
				display.setText(display.getText() + " )");
			} else {
				display.setText(display.getText() + "( ");
			}
		} else if (ButtonValue == add) {
			display.setText(display.getText() + " + ");
		} else if (ButtonValue == sub) {
			display.setText(display.getText() + " - ");
		} else if (ButtonValue == mult) {
			display.setText(display.getText() + " * ");
		} else if (ButtonValue == div) {
			display.setText(display.getText() + " / ");
		} else if (ButtonValue == mod) {
			display.setText(display.getText() + " % ");
		} else if (ButtonValue == del) {
			if (display.getText().contains("(") || display.getText().contains(")")) {
				if (display.getText().charAt(display.getText().length() - 2) == '(' || display.getText().charAt(display.getText().length() - 2) == ')') {
					display.deleteText(display.getText().length() - 2, display.getText().length());
				} else if (display.getText().charAt(display.getText().length() - 1) == ' ') {
					display.deleteText(display.getText().length() - 3, display.getText().length());
				} else if (display.getText().length() == 1) {
					display.clear();
				} else {
					display.deleteText(display.getText().length() - 1, display.getText().length());
				}
			} else {
				if (display.getText().charAt(display.getText().length() - 1) == ' ') {
					display.deleteText(display.getText().length() - 3, display.getText().length());
				} else if (display.getText().length() == 1) {
					display.clear();
				} else {
					display.deleteText(display.getText().length() - 1, display.getText().length());
				}
			}
		} else if (ButtonValue == reset) {
			display.setText("");
		}

	}

	@FXML
	public void handleButtonActionCalc(ActionEvent event) {
		String input = display.getText();
		input = input.replace(" ", "");
		System.out.println(input);

		if (calcTree.init(input)) {
			String agregateInString = String.valueOf(calcTree.solve());
			clearText = true;
			if (agregateInString.charAt(agregateInString.length() - 1) == '0') {
				try {
					display.setText(String.valueOf((int) calcTree.solve()));
					System.out.println(String.valueOf((int) calcTree.solve()));
				} catch (Exception ex) {
					double agregate = (double) calcTree.solve();
					display.setText(String.valueOf(Math.round(agregate)));
					System.out.println(String.valueOf(Math.round(agregate)));
				}
				// int agregate = Math.round((int) calcTree.solve());
				// display.setText(String.valueOf((int) calcTree.solve()));
			} else {
				display.setText(agregateInString);
				System.out.println(agregateInString);
			}
			// display.setText(String.valueOf(calcTree.solve()));
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
}
