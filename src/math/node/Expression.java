package math.node;

public abstract class Expression implements Cloneable {

	protected boolean parenthesis = false; // boolean um zu Zeigen ob Klammern um den ausdruck bestehen

	public void setParens(boolean bool) {
		parenthesis = bool;
	}

	public boolean isParens() {
		return parenthesis;
	}

	/**
	 * arbeitet sich den Tree runter
	 * 
	 * @return Zahl von Integer oder Double basierend auf dem vorherigen Ausdruck.
	 */

	abstract public Number calculate();

	abstract public String toString();

	public Object clone() throws CloneNotSupportedException {
		return (Expression) super.clone();
	}

	// Methode um zu schauen ob Tree richtig aufgebaut ist
	abstract public boolean checkTree();

}
