package math.tree;

import java.util.HashSet;
import java.util.LinkedList;

public class StringScanner implements Cloneable {
	
	private LinkedList<String> tokenList = new LinkedList();
	private HashSet<Character> delimSet = new HashSet();
	private HashSet<Character> specCharSet = new HashSet();
	private boolean skipWhitespace = false;
	
	public void skipStuff() {
		skipWhitespace = true;
	}
	
	public void addSpecialChar(char specialChar) {
		specCharSet.add(specialChar);
	}
	
	public void addSpecialChar(char[] specialCharArray) {
	      for(char ch : specialCharArray)
	         specCharSet.add(ch); 
	}
	
	 public LinkedList<String> scan(String inputStr) {
		 char ch;
	      String token = "";
	      for(int i = 0; i < inputStr.length(); i++) {
	         ch = inputStr.charAt(i);
	         
	         if(isDelim(ch)) {
	            saveToken(token);
	            token = "";
	         } else if(specCharSet.contains(ch)) {
	            saveToken(token);
	            saveToken(Character.toString(ch));
	            token = "";
	         } else {
	            token = token + ch;
	         }
	            
	      }
	      
	      saveToken(token);
	      
	      LinkedList<String> copyTokenList = tokenList;
	      tokenList = new LinkedList();
	      
	      return copyTokenList;
	 }
	 
	 private void saveToken(String token) 
	   {
	      if(token.isEmpty() == false)
	         tokenList.add(token);
	   }
	 
	 private boolean isDelim(char ch) {
	      return (skipWhitespace && Character.isWhitespace(ch)) || delimSet.contains(ch);
	 }
	 
	 public Object clone() throws CloneNotSupportedException
	   {
	      StringScanner clone = (StringScanner) super.clone();
	      clone.tokenList = (LinkedList) tokenList.clone();
	      clone.delimSet = (HashSet) delimSet.clone(); 
	      clone.specCharSet = (HashSet) specCharSet.clone();
	      
	      return clone;
	   }

}
