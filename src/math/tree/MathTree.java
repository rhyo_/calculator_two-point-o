package math.tree;

import java.util.LinkedList;

public class MathTree implements Cloneable {

	private math.node.Expression rootNode = null;
	private StringScanner strScanner = new StringScanner();
	private math.node.Factory nodeFactory = new math.node.Factory();

	public MathTree() {
		// scanner setup
		strScanner.skipStuff();
		// alles wird überprüft außer '-' das wird bei cleanStrList() gemacht
		char[] specialChars = { '(', ')', '+', '*', '/', '^' };
		strScanner.addSpecialChar(specialChars);
	}

	public boolean init(String mathStatement) {

		LinkedList<String> strList = strScanner.scan(mathStatement);

		cleanStrList(strList);
		if (buildTree(strList)) {
			if (rootNode.checkTree()) {
				return true;
			} else {
				System.out.println("Invalid: Unknown expression");
			}
		}

		rootNode = null;
		return false;
	}

	private void cleanStrList(LinkedList<String> strList) {
		// neuer scanner für '-'
		StringScanner negScanner = new StringScanner();
		negScanner.addSpecialChar('-');

		// Liste mit operatoren
		String opStr = "+-*^/";

		String tempStr;
		LinkedList<String> newList = new LinkedList();

		for (int i = 0; i < strList.size(); i++) {
			tempStr = strList.get(i);

			// String mit minus Zeichen wierden überpüft ob minus und minus zu plus werden.
			if (tempStr.length() > 1 && tempStr.contains("-")) {
				newList = negScanner.scan(tempStr);

				if (newList.get(0).equals("-") && !newList.get(1).equals("-")) {
					newList.removeFirst();
					String newStr = "-" + newList.removeFirst();
					newList.addFirst(newStr);
				}

				// Überprüfung auf negative Zahlen und subtraktionen in newList.
				for (int k = 2; k < newList.size(); k++) {
					if (newList.get(k - 2).equals("-") && newList.get(k - 1).equals("-")
							&& !newList.get(k).equals("-")) {
						newList.remove(k - 1);
						String newStr = "-" + newList.remove(k - 1);
						newList.add(k - 1, newStr);
						k--;
					}
				}

				// ersetze tempStr in strList mit newList.
				strList.remove(i);
				strList.addAll(i, newList);

				i += newList.size() - 1;
			}
			// Hinzufügen von multiplication vor Klammern
			else if (tempStr.equals("(") && i > 0) {
				String prevStr = strList.get(i - 1);

				if (!opStr.contains(prevStr) && prevStr != "(") {
					strList.add(i, "*");
					i++;
				}
			}
			// Hinzufügen von multiplication nach Klammern
			else if (tempStr.equals(")") && i < strList.size() - 1) {
				String nextStr = strList.get(i + 1);

				if (!opStr.contains(nextStr) && nextStr != ")") {
					strList.add(i + 1, "*");
					i++;
				}
			}
		}
	}

	private boolean buildTree(LinkedList<String> strTokens) {
		rootNode = buildTree(strTokens, false);

		if (rootNode == null)
			return false;
		else
			return true;
	}

	// Funktion, die trees von mathematischen math nodes erstellt. Gibt null zurück, wenn leer oder ungültig.
	private math.node.Expression buildTree(LinkedList<String> strTokens, boolean isParens) {
		String token;
		math.node.Expression rootNode = null;
		// mathNode.Expression lastNode = null;
		math.node.Expression newNode = null;

		while (strTokens.isEmpty() == false) {
			token = strTokens.poll();

			// geschlossene Klammern
			if (token.equals(")")) {
				if (isParens && rootNode == null) {
					System.out.println("Invalid: Empty parenthesis");
					return null;
				} else if (!isParens) {
					System.out.println("Invalid: Missing \"(\"");
					return null;
				} else {
					rootNode.setParens(true);
					return rootNode;
				}
			}

			// offene Klammern
			if (token.equals("(")) {
				newNode = buildTree(strTokens, true);
				if (newNode == null) {
					return null;
				}

				rootNode = insertNode(rootNode, newNode);

				if (rootNode == null) {
					return null;
				} else {
					continue;
				}
			}

			// Neue node kreieren und in Tree einsetzten
			newNode = nodeFactory.buildNode(token);
			if (newNode == null) {
				System.out.println("Invalid: Unknown expression \"" + token + "\"");
				return null;
			} else {
				rootNode = insertNode(rootNode, newNode);
			}

			if (rootNode == null) {
				return null;
			}

		}

		// Überprüfung auf endklammer
		if (isParens) {
			System.out.println("Invalid: Missing \")\"");
			return null;
		} else {
			return rootNode;
		}
	}

	// neue node in tree von rootNode
	private math.node.Expression insertNode(math.node.Expression rootNode, math.node.Expression newNode) {
		// wenn keine root node exestiert neue node wird rootNode
		if (rootNode == null) {
			return newNode;
		} else if (newNode == null) { // wenn keine neue node return
			return rootNode;
		} else if (newNode instanceof math.node.Operator && !newNode.isParens()) { // operator in tree hinzufügen ohne klammern

			math.node.Operator newOperator = (math.node.Operator) newNode;
			math.node.Operator parent;

			// ist rootNode operator?
			if (rootNode instanceof math.node.Operator) {
				parent = (math.node.Operator) rootNode;
			} else {
				newOperator.setLeftNode(rootNode);
				return newOperator;
			}

			// neuer platz in tree um operator zu plazieren mit vergleichen(Preorität)
			if (parent.getPrecedence() <= newOperator.getPrecedence()) {
				newOperator.setLeftNode(parent);
				return newOperator;
			}

			while (parent.getPrecedence() > newOperator.getPrecedence() && parent.getRightNode() != null
					&& parent.getRightNode() instanceof math.node.Operator) {
				parent = (math.node.Operator) parent.getRightNode();
			}

			// wert zwischen Operatoren da?
			if (parent.getRightNode() == null) {
				System.out.println("Invalid: Missing value between two operators");
				return null;
			} else {
				newOperator.setLeftNode(parent.getRightNode());
				parent.setRightNode(newOperator);
				return rootNode;
			}
		} else
		// Plazieren von Wert oder klammer ganz rechts im tree
		// wenn es belegt ist invalid da Zwei nummern in reie
		{
			math.node.Operator parent;

			if (rootNode instanceof math.node.Operator) {
				parent = (math.node.Operator) rootNode;
			} else {
				System.out.println("Invalid: Missing operator between " + rootNode + " and " + newNode);
				return null;
			}

			while (parent.getRightNode() != null) {
				if (parent.getRightNode() instanceof math.node.Operator) {
					parent = (math.node.Operator) parent.getRightNode();
				} else {
					System.out.println("Invalid: Missing operator between " + parent.getRightNode() + " + " + newNode);
					return null;
				}
			}

			parent.setRightNode(newNode);
			return rootNode;
		}
	}

	// ruft methode math.node.Expression.calculate() auf, wenn tree leer return null
	public Number solve() {
		if (rootNode == null) {
			return null;
		} else {
			return rootNode.calculate();
		}
	}

	// ruft methode math.node.Expression.toString() auf, wenn tree leer return null
	public String toString() {
		if (rootNode == null) {
			return "";
		} else {
			return rootNode.toString();
		}
	}

	public Object clone() throws CloneNotSupportedException {
		MathTree clone = (MathTree) super.clone();
		clone.nodeFactory = (math.node.Factory) nodeFactory.clone();
		clone.rootNode = (math.node.Expression) rootNode.clone();

		return clone;
	}
}
